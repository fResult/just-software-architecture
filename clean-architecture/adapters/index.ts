import ELearningAdapterImpl, { IELearningAdapter } from "./Elearning.adapter";
import PaymentAdapterImpl, { IPaymentAdapter } from "./Payment.adapter";

export {
    ELearningAdapterImpl,
    IELearningAdapter,
    IPaymentAdapter,
    PaymentAdapterImpl
};
