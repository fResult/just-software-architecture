import { Course, Payment, Student } from "../entities";

export interface IELearningAdapter {
    ViewCourse(): void;

    RegisterCourse(course: Course, student: Student): void;
}

export default class ELearningAdapterImpl implements IELearningAdapter {
    public ViewCourse(): void {
        console.log("The Course is viewed");
        throw new Error("Method not implemented.");
    }
    public RegisterCourse(course: Course, student: Student): void {
        console.log(`${course.name} Course is viewed by ${[student.firstName, student.lastName].join(' ')}`)
        throw new Error("Method not implemented.");
    }
}
