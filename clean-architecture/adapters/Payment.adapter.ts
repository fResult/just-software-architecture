import { PaymentResult } from "../constants";
import { Payment } from "../entities";

export interface IPaymentAdapter {
    Pay(payment: Payment): PaymentResult;
}

export default class PaymentAdapterImpl implements IPaymentAdapter {
    public Pay(payment: Payment): PaymentResult {
        return payment.amount ? PaymentResult.Success : PaymentResult.Failed;
    }
}
