import { IELearningAdapter, IPaymentAdapter } from "../adapters";
import { PaymentResult } from "../constants";
import { Course, Payment, Student } from "../entities";

export default class CourseUseCases {
    constructor(
        private paymentAdapter: IPaymentAdapter,
        private eLearningAdapter: IELearningAdapter
    ) {}

    public BuyCourse(course: Course, student: Student, payment: Payment): void {
        if (!course.isActive) {
            // TODO: implement `CourseNotActiveException`
            throw new Error("CourseNotActiveException");
        }
        if (!student.Blacklisted()) {
            // TODO: implement `CannotBuyException`
            throw new Error("CannotBuyException");
        }
        if (this.paymentAdapter.Pay(payment) === PaymentResult.Success) {
            this.eLearningAdapter.RegisterCourse(course, student);
        }
    }
}
