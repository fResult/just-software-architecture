# Clean Achitecture

![Clean Architecture Concept](https://gitlab.com/fResult/just-software-architecture/uploads/2ed86ae1dfa783e59846792a67feaeda/image.png)

ตาม concept ของ Clean Architecture แล้ว  
ข้างนอก (วงสีฟ้า) ต้อง depend on ข้างใน (วงในๆ เข้ามา)  เท่านั้น
แต่ข้างในต้องไม่ depend on ข้างนอก

![Clean Achitecture Applying on E-Learning use case](https://gitlab.com/fResult/just-software-architecture/uploads/e9827ed090de9b96176661650cff2949/image.png)

ถ้าดูจาก code ที่ได้เขียนไว้จะเป็น...
- **Use-cases** (ในบาง project อาจเรียกว่า Service)  
  ซึ่งถ้าใช้ Dependency Inversion ดีๆ แล้วเมื่อเราเปลี่ยน Logic ที่วงชั้นในอย่าง Entity  
  แล้วจะทำให้ Use-cases หรือ Service ต้องไม่แก้ไขอะไรตามทั้งนั้น (Core ไม่ควรโดนเปลี่ยนแปลงตามการเปลี่ยนแปลงของชั้นใน)
- **Adapter** เป็นส่วนที่เชื่อมต่อกับ 3rd-party เช่น  
  เช่น External Library, Database, Mainframe (maybe another MicroService)  
  โดยจะถูก Inject มาจาก Controller ผ่าน contructor injection
- **Entity** ต่างๆ ก็ถูก inject ผ่าน method injection


### Key takeaways
- ทุกๆ คนที่ทำงานใน layer ใดๆ ต้องเข้าใจ Domain, Entity, และ Use-cases ของตัวเอง
- และดังนั้น developer ทุกคนต้องเข้าใจ Domain และ Use-cases ให้ได้ แปลว่าคนที่ทำงานกับ Software ต้องเข้าใจในสิ่งที่กำลังทำเสมอ
- แต่ก็ไม่จำเป็นต้องเข้าใจ 3rd-party  
  (เช่น คนที่ทำในส่วน Payment ก็ไม่จำเป็นต้องเข้าใจในส่วน E-Learning ก็ได้)
