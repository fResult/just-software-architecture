import Course from "./Course.entity";
import Payment from "./Payment.entity";
import Student from "./Student.entity";

export { Course, Payment, Student };
