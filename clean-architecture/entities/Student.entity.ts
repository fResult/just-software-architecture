export default class Student {
  #firstName: string
  #lastName: string

  get firstName(): string {
    return this.#firstName
  }
  set firstName(val: string) {
    this.#firstName = val
  }

  get lastName(): string {
    return this.#lastName
  }
  set lastName(val: string) {
    this.#lastName = val
  }

  public Blacklisted(): boolean {
    return false
  }
}
