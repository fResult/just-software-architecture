export default class Course {
    public name: string;
    public isActive: boolean;
}
